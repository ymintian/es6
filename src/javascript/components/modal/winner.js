import {showModal} from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const {name} = fighter;
  const bodyElement = createElement({ tagName: 'div', className: 'winner__text' });
  bodyElement.innerText = name;
  const title = 'Winner';
  showModal({title, bodyElement});
}
