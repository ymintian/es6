import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';

export function createFightersSelector() {
  let selectedFighters = [];

  return async (event, fighterId) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    console.log('selected',selectedFighters)
    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId) {
  const fighterInfo = await fighterService.getFighterDetails(fighterId);
  fighterDetailsMap.set(fighterId, fighterInfo);
  console.log('fighter info map',fighterDetailsMap);
  return fighterInfo;
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
}

function renderSelectedFighters(selectedFighters) {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  let firstPreview, secondPreview, versusBlock;
  try{
  firstPreview = createFighterPreview(playerOne, 'left');
  versusBlock = createVersusBlock(selectedFighters);
  secondPreview = createFighterPreview(playerTwo, 'right');
  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock,secondPreview);
  }
  catch(e){
    console.log('no second fighter, first :', firstPreview);
    fightersPreview.append(firstPreview, versusBlock);
  }
}

function createVersusBlock(selectedFighters) {
  selectedFighters.filter(Boolean).length
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters) {
  renderArena(selectedFighters);
}
