import { createElement } from '../helpers/domHelper';
import { fighters } from '../helpers/mockData';
import { fight } from './fight';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImg = createFighterImage(fighter);
  const {name,health,attack,defense} = fighter;

  const fighterName = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });
  fighterName.innerHTML = name;
  const fighterHealth = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });
  fighterHealth.innerHTML = `Health: ${health}`;
  const fighterAttack = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
    
  });
  fighterAttack.innerHTML = `Attack: ${attack}`;
  const fighterDefense = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });
  fighterDefense.innerHTML = `Defense: ${defense}`;

  fighterElement.append(fighterImg, fighterName, fighterHealth, fighterAttack, fighterDefense);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  console.log("fighter prew",fighter)
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
