import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  console.log({firstFighter,secondFighter});

  let fightInfo = {
    keysHistory: [], 
    inAttack: null, 
    inDefense: null, 
    firstFighter:{...firstFighter},
    secondFighter:{...secondFighter}
  };

  function handleKeyDown(e) {
    const {code} = e;
    console.log({code,fightInfo});
    switch(code){
      case `${controls.PlayerOneAttack}`:
        console.log('case 1attack');
        fightInfo.keysHistory.push(controls.PlayerOneAttack.slice(-1));
        fightInfo.inAttack = firstFighter;
        fightInfo.inDefense = secondFighter;
        const secondDamage = getDamage(firstFighter,secondFighter);
        fightInfo.secondFighter.health = fightInfo.secondFighter.health - secondDamage;
        const rightIndicator = document.getElementById('right-fighter-indicator');
        const rightIndicatorWidth = rightIndicator.clientWidth;
        rightIndicator.style.width = rightIndicatorWidth-(secondDamage*rightIndicatorWidth/fightInfo.secondFighter.health)+'px';
        
        if(fightInfo.secondFighter.health <= 0) {
          winner = firstFighter;
          rightIndicator.style.width = '0px';
          document.removeEventListener('keydown', handleKeyDown);
          return
        }
        console.log({secondDamage,fightInfo});
        break;
      case `${controls.PlayerTwoAttack}`:
        console.log('case 2attack')
        fightInfo.keysHistory.push(controls.PlayerTwoAttack.slice(-1));
        fightInfo.inAttack = secondFighter;
        fightInfo.inDefense = firstFighter;
        const firstDamage = getDamage(secondFighter,firstFighter);
        fightInfo.firstFighter.health = fightInfo.firstFighter.health - firstDamage;
        const leftIndicator = document.getElementById('left-fighter-indicator');
        const leftIndicatorWidth = leftIndicator.clientWidth;
        leftIndicator.style.width = leftIndicatorWidth-(firstDamage*leftIndicatorWidth/fightInfo.firstFighter.health)+'px';
        if(fightInfo.firstFighter.health <= 0) {
          winner = secondFighter;
          leftIndicator.style.width = '0px';
          document.removeEventListener('keydown', handleKeyDown);
          return
        }
        console.log({firstDamage,fightInfo});
        break;
      case `${controls.PlayerOneBlock}`:
        console.log('case 1block')
        fightInfo.keysHistory.push(controls.PlayerOneBlock.slice(-1));
        break;
      case `${controls.PlayerOneBlock}`:
        console.log('case 2block')
        fightInfo.keysHistory.push(controls.PlayerOneBlock.slice(-1));
        break;
      case `${controls.PlayerOneCriticalHitCombination}`:
        console.log('case 1combo')
        fightInfo.keysHistory.push(controls.PlayerOneCriticalHitCombination.slice(-1));
        break;
      case `${controls.PlayerTwoCriticalHitCombination}`:
        console.log('case 2combo')
        fightInfo.keysHistory.push(controls.PlayerTwoCriticalHitCombination.slice(-1));
        break;
      default:
       console.log('default')
       return null;
    }
  }

  return new Promise((resolve) => {
    document.addEventListener('keydown', handleKeyDown);
    let winner = null;
    function handleKeyDown(e) {
      const {code} = e;
      console.log({code,fightInfo});
      switch(code){
        case `${controls.PlayerOneAttack}`:
          console.log('case 1attack');
          fightInfo.keysHistory.push(controls.PlayerOneAttack.slice(-1));
          fightInfo.inAttack = firstFighter;
          fightInfo.inDefense = secondFighter;
          const secondDamage = getDamage(firstFighter,secondFighter);
          fightInfo.secondFighter.health = fightInfo.secondFighter.health - secondDamage;
          const rightIndicator = document.getElementById('right-fighter-indicator');
          const rightIndicatorWidth = rightIndicator.clientWidth;
          rightIndicator.style.width = rightIndicatorWidth-(secondDamage*rightIndicatorWidth/fightInfo.secondFighter.health)+'px';
          
          if(fightInfo.secondFighter.health <= 0) {
            winner = firstFighter;
            rightIndicator.style.width = '0px';
            document.removeEventListener('keydown', handleKeyDown);
            resolve(winner);
          }
          console.log({secondDamage,fightInfo});
          break;
        case `${controls.PlayerTwoAttack}`:
          console.log('case 2attack')
          fightInfo.keysHistory.push(controls.PlayerTwoAttack.slice(-1));
          fightInfo.inAttack = secondFighter;
          fightInfo.inDefense = firstFighter;
          const firstDamage = getDamage(secondFighter,firstFighter);
          fightInfo.firstFighter.health = fightInfo.firstFighter.health - firstDamage;
          const leftIndicator = document.getElementById('left-fighter-indicator');
          const leftIndicatorWidth = leftIndicator.clientWidth;
          leftIndicator.style.width = leftIndicatorWidth-(firstDamage*leftIndicatorWidth/fightInfo.firstFighter.health)+'px';
          if(fightInfo.firstFighter.health <= 0) {
            winner = secondFighter;
            leftIndicator.style.width = '0px';
            document.removeEventListener('keydown', handleKeyDown);
            resolve(winner);
          }
          console.log({firstDamage,fightInfo});
          break;
        case `${controls.PlayerOneBlock}`:
          console.log('case 1block')
          fightInfo.keysHistory.push(controls.PlayerOneBlock.slice(-1));
          break;
        case `${controls.PlayerOneBlock}`:
          console.log('case 2block')
          fightInfo.keysHistory.push(controls.PlayerOneBlock.slice(-1));
          break;
        case `${controls.PlayerOneCriticalHitCombination}`:
          console.log('case 1combo')
          fightInfo.keysHistory.push(controls.PlayerOneCriticalHitCombination.slice(-1));
          break;
        case `${controls.PlayerTwoCriticalHitCombination}`:
          console.log('case 2combo')
          fightInfo.keysHistory.push(controls.PlayerTwoCriticalHitCombination.slice(-1));
          break;
        default:
         console.log('default')
         return null;
      }
    }
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const {attack} = fighter;
  const hitPower = attack * criticalHitChance;
  return hitPower
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const {defense} = fighter;
  const blockPower = defense * dodgeChance;
  return blockPower;
}
